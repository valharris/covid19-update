localStorage.setItem("screeningName", "");

document.querySelector("body").onresize = function() {
    $('#sidebar-xs').modal('hide');
};

if(!localStorage.getItem("isScreeningDisplayed")) {
    $('#screening').modal('show');
    localStorage.setItem("isScreeningDisplayed", true);
}

//localStorage.clear();

//-------------------------------------------------------------------

if(document.querySelector("#start-name")) {
    document.querySelector("#start-name").onfocus = function() {
        document.querySelector("#start-name-err").innerHTML = "";
    };
}

if(document.querySelector("#start-btn")) {
    document.querySelector("#start-btn").onclick = function() {
        if(document.querySelector("#start-name").value != "") {
            //alert(typeof localStorage.getItem("screeningResult"));
            localStorage.setItem("screeningName", document.querySelector("#start-name").value);
            $('#cet-start').hide();
            $('#cet-q1').show();
        } else {
            $('#start-name-err').html("Please enter your name.");
        }
    };
}

document.querySelector("#cet-q1-back").onclick = function() {
    $('#cet-q1').hide();
    $('#cet-start').show();
};

document.querySelector("#cet-q2-back").onclick = function() {
    $('#cet-q2').hide();
    $('#cet-q1').show();
};

document.querySelector("#cet-q3-back").onclick = function() {
    $('#cet-q3').hide();
    $('#cet-q2').show();
};

document.querySelector("#cet-q4-back").onclick = function() {
    $('#cet-q4').hide();
    $('#cet-q2').show();
};

document.querySelector("#cet-q5-back").onclick = function() {
    $('#cet-q5').hide();
    $('#cet-q4').show();
};

document.querySelector("#cet-q6-back").onclick = function() {
    $('#cet-q6').hide();
    $('#cet-q1').show();
};

document.querySelector("#cet-q7-back").onclick = function() {
    $('#cet-q7').hide();
    $('#cet-q6').show();
};

document.querySelector("#cet-q8-back").onclick = function() {
    $('#cet-q8').hide();
    $('#cet-q7').show();
};

document.querySelector("#cet-q9-back").onclick = function() {
    $('#cet-q9').hide();
    $('#cet-q7').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q1-traveller").onclick = function() {
    $('#cet-q1').hide();
    $('#cet-q2').show();
};

document.querySelector("#cet-q1-non-traveller").onclick = function() {
    $('#cet-q1').hide();
    $('#cet-q6').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q2-yes").onclick = function() {
    $('#cet-q2').hide();
    $('#cet-q3').show();
};

document.querySelector("#cet-q2-no").onclick = function() {
    $('#cet-q2').hide();
    $('#cet-q4').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q3-yes").onclick = function() {
    $('#cet-q3').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#pui').show();
};

document.querySelector("#cet-q3-no").onclick = function() {
    $('#cet-q3').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#pum').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q4-yes").onclick = function() {
    $('#cet-q4').hide();
    $('#cet-q5').show();
};

document.querySelector("#cet-q4-no").onclick = function() {
    $('#cet-q4').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#npp').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q5-yes").onclick = function() {
    $('#cet-q5').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#pui').show();
};

document.querySelector("#cet-q5-no").onclick = function() {
    $('#cet-q5').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#pum').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q6-yes").onclick = function() {
    $('#cet-q6').hide();
    $('#cet-q7').show();
};

document.querySelector("#cet-q6-no").onclick = function() {
    $('#cet-q6').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#npp').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q7-yes").onclick = function() {
    $('#cet-q7').hide();
    $('#cet-q8').show();
};

document.querySelector("#cet-q7-no").onclick = function() {
    $('#cet-q7').hide();
    $('#cet-q9').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q8-yes").onclick = function() {
    $('#cet-q8').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#pui').show();
};

document.querySelector("#cet-q8-no").onclick = function() {
    $('#cet-q8').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#npp').show();
};

//-------------------------------------------------------------------

document.querySelector("#cet-q9-yes").onclick = function() {
    $('#cet-q9').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#pui').show();
};

document.querySelector("#cet-q9-no").onclick = function() {
    $('#cet-q9').hide();
    $('.in-name').html(localStorage.getItem("screeningName") + ', your result is:');
    $('#npp').show();
};

//-------------------------------------------------------------------

// function main() {
//     let t1 = false;
//     let t2 = false;
//     let t3 = false;
//     let t4 = false;
//     let t5 = false;
//     let t6 = false;
//     let t7 = false;
//     let t8 = false;
//     let t9 = false;
//     let t10 = false;

//     let input;

//     do {
//         input = prompt("1Traveller or Patient?\n(enter: 'traveller' / 'non-traveller')");

//         if(input == "traveller") {
//             t1 = true;
//             do {
//                 input = prompt("2Have you travelled in the past 14 days to countries with local transmission and risk of important?\n(enter: 'y' / 'n')");

//                 if(input == "y") {
//                     t2 = true;
//                     do {
//                         input = prompt("3Do you have the following symptoms (ACUTE RESPIRATORY ILLNESS): \nA.Fever ( greater than 38.0 C) \nB.Cough OR shortness of breath OR other respiratory symptoms OR \nC.Diarrhea.\n(enter: 'y' / 'n')");

//                         if(input == "y") {
//                             t3 = true;
//                             alert("Patient Under Investigation (PUI).");
//                         } else if(input == "n") {
//                             t3 = true;
//                             alert("Person Under Monitoring (PUM) \"Home Quarantine\"");
//                         }
//                     } while(t3 == false);
//                 } else if(input == "n") {
//                     t2 = true;
//                     do {
//                         input = prompt("4Have you: (1 or more) \nA. Providing direct care for COVID-19 patient \nB. Working together or staying in the same close environment of a COVID-19 \nC. Travelling together with COVID-19 patient in any kind of conveyance \nD. Living in the same household as a COVID-19 patient within a 14-day period.\n(enter: 'y' / 'n')");

//                         if(input == "y") {
//                             t4 = true;
//                             do {
//                                 input = prompt("5Do you have the following symptoms (ACUTE RESPIRATORY ILLNESS): \nA.Fever ( greater than 38.0 C) \nB.Cough OR shortness of breath OR other respiratory symptoms OR \nC.Diarrhea.\n(enter: 'y' / 'n')");

//                                 if(input == "y") {
//                                     t5 = true;
//                                     alert("Patient Under Investigation (PUI).");
//                                 } else if(input == "n") {
//                                     t5 = true;
//                                     alert("Person Under Monitoring (PUM) \"Home Quarantine\"");
//                                 }
//                             } while(t5 == false);
//                         } else if(input == "n") {
//                             t4 = true;
//                             alert("Not PUM or PUI.");
//                         }
//                     } while(t4 == false);
//                 }
//             } while(t2 == false);
//         } else if(input == "non-traveller") {
//             t1 = true;
//             do {
//                 input = prompt("6Do you have the following symptoms (ACUTE RESPIRATORY ILLNESS): \nA.Fever ( greater than 38.0 C) \nB.Cough OR shortness of breath OR other respiratory symptoms OR \nC.Diarrhea.\n(enter: 'y' / 'n')");

//                 if(input == "y") {
//                     t6 = true;
//                     do {
//                         input = prompt("7Have you: (1 or more) \nA. Providing direct care for COVID-19 patient \nB. Working together or staying in the same close environment of a COVID-19 \nC. Travelling together with COVID-19 patient in any kind of conveyance \nD. Living in the same household as a COVID-19 patient within a 14-day period.\n(enter: 'y' / 'n')");

//                         if(input == "y") {
//                             t7 = true;
//                             do {
//                                 input = prompt("8Did the symptoms occur within 14 days of exposure?\n(enter: 'y' / 'n')");

//                                 if(input == "y") {
//                                     t8 = true;
//                                     alert("Patient Under Investigation (PUI).");
//                                 } else if(input == "n") {
//                                     t8 = true;
//                                     alert("Not PUM or PUI.");
//                                 }
//                             } while(t8 == false);
//                         } else if(input == "n") {
//                             t7 = true;
//                             do {
//                                 input = prompt("9Do you or anybody in your household have severe acute respiratory infection or a typical pneumonia that requires hospitalization?\n(enter: 'y' / 'n')");

//                                 if(input == "y") {
//                                     t9 = true;
//                                     alert("Patient Under Investigation (PUI).");
//                                 } else if(input == "n") {
//                                     t9 = true;
//                                     alert("Not PUM or PUI.");
//                                 }
//                             } while(t9 == false);
//                         }
//                     } while(t7 == false);
//                 } else if(input == "n") {
//                     t6 = true;
//                     alert("Not PUM or PUI.");
//                 }
//             } while(t6 == false);
//         }
//         //alert(t1);
//     } while(t1 == false);
// }
